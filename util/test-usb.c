/* Keyboard controller: an utility to test keyboard;
   it could be used to write down raw key scancodes
   ((column << 3) | (raw)), and so on.

Copyright (C) 2016 Ineiev <ineiev@gnu.org>, super V 93

This file is a part of miguel the keyboard controller.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.*/
#include <stdio.h>
#include <stdlib.h>
#include <libusb.h>
#include <time.h>
#include <unistd.h>
enum more_usb_constants
{
  USB_HID_GET_REPORT = 1,
  USB_HID_SET_REPORT = 9,
  USB_HID_GET_INPUT = 1,
  USB_HID_GET_FEATURE = 3,
};
static void
read_data (libusb_device_handle *dev)
{
  int i = 0, res, N = 0, j, n = 0;
  unsigned char buf[128];
  time_t t0, t, t1;
  long long s = 0;

  t1 = t0 = time (NULL);
  while (1)
    {
      res = libusb_control_transfer (dev,
              LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_DEVICE
                | LIBUSB_ENDPOINT_IN,
              USB_HID_GET_REPORT, USB_HID_GET_FEATURE << 8, 0,
              buf, sizeof (buf), 500u);
      if (res <= 0)
        continue;
      N += res;
      i++;
      fwrite (buf + 1, 1, res - 1, stdout);
      for (j = 1; j < res; j++)
        {
          if (0 && buf[j] > 20)
            continue;
          s += buf[j];
          n++;
        }
      t = time (NULL);
      if (t == t1)
        continue;
      t1 = t;
      t -= t0;
      fprintf (stderr, "%llus %.3f r/s, %.3f b/s mean %.3f\n",
               (unsigned long long) t, ((double)i) / t,
               ((double)N) / t, ((double)s) / n);
    }
}
static void
write_period (int x, libusb_device_handle *dev)
{
  unsigned char buf[0x11];
  int res;

  buf[0] = x;
  buf[1] = 1;
  res = libusb_control_transfer (dev,
          LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_DEVICE,
          USB_HID_SET_REPORT, 0, 0, buf, 2, 500u);

  fprintf (stderr, "wrote %i of 2\n", res);
}
static void
write_leds (int x, libusb_device_handle *dev)
{
  int res, i;
  unsigned char buf[9];
  time_t t = time (NULL);

  *buf = x;
  fprintf (stderr, "writing %X\n", x);
  res = libusb_control_transfer (dev,
          LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_DEVICE,
          USB_HID_SET_REPORT, 0, 0, buf, 1, 500u);
  if (res != 1)
    fprintf (stderr, "transfer failed %i\n", res);
  while (time (NULL) - t < 2)
    {
      res = libusb_control_transfer (dev,
              LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_DEVICE
                | LIBUSB_ENDPOINT_IN,
              USB_HID_GET_REPORT, USB_HID_GET_INPUT << 8, 0, buf, 8, 500u);
      if (0 >= res)
        continue;
      fprintf (stderr, "read %i:", res);
      for (i = 0; i < res; i++)
        fprintf (stderr, " %2.2X", buf[i]);
      fprintf (stderr, "\n");
    }
}
int
main (int argc, char**argv)
{
  libusb_device_handle *dev = NULL;
  int i;

  if (libusb_init (NULL))
    {
      fprintf (stderr, "failed to init libusb\n");
      return 1;
    }
  libusb_set_debug (NULL, 3);

  dev = libusb_open_device_with_vid_pid (NULL, 0x16C0, 0x27DB);
  if (!dev)
    {
      fprintf (stderr, "device not found\n");
      libusb_exit (NULL);
      return 0;
    }

  libusb_detach_kernel_driver (dev, 0);
  libusb_claim_interface (dev, 0);
  libusb_reset_device (dev);
  if (argc > 1)
    {
      if (1 == sscanf (argv[1], "%i", &i))
        write_period (i,  dev);
      else
        read_data (dev);
    }
  else for (i = 0; i < 0x11; i++)
    write_leds (i, dev);
  libusb_release_interface (dev, 0);
  libusb_attach_kernel_driver (dev, 0);
  libusb_close (dev);
  libusb_exit (NULL);
  return 0;
}

