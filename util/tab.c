/* Keyboard controller: an utility to transform a list of key codes
   to a sequence of numbers suitable to insert into a C program.

Copyright (C) 2016 Ineiev <ineiev@gnu.org>, super V 93

This file is a part of miguel the keyboard controller.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.*/
#include<stdio.h>
int
main(void)
{
  unsigned char tab[1<<8];
  unsigned x, y;
  for (x = 0; x < sizeof (tab); x++)
    tab[x] = 3;/* ErrorUndefined */
  while (2 == scanf ("%X %X", &x, &y))
    tab[x & 0xFF] = y;
  for (x = 0; x < sizeof (tab) - 1; x++)
    printf ("0x%2.2X,%c", tab[x], ((x & 7) == 7)? '\n': ' ');
  printf ("0x%2.2X\n", tab[sizeof (tab) - 1]);
  return 0;
}
