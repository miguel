66 04
73 05
5C 06
5E 07
60 08
76 09
75 0A
55 0B
50 0C
56 0D
4E 0E
2E 0F
54 10
53 11
30 12
48 13
68 14
78 15
6E 16
77 17
58 18
74 19
70 1A
6C 1B
57 1C
64 1D
61 1E
69 1F
59 20
71 21
72 22
52 23
51 24
49 25
29 26
41 27
3C 28
65 29
3F 2A
67 2B
0D 2C
42 2D
4A 2E
47 2F
4F 30
3E 31
46 33
45 34
62 35
4C 36
2C 37
43 38
6F 39
6A 3A
5A 3B
5F 3C
5D 3D
79 3E
4D 3F
2F 40
2A 41
3A 42
39 43
3D 44
3B 45
31 46
38 47
80 48
12 49
22 4A
1A 4B
0A 4C
21 4D
19 4E
13 4F
23 50
0B 51
25 52
0C 53
14 54
1C 55
1B 56
28 57
26 58
0E 59
16 5A
1E 5B
0F 5C
17 5D
1F 5E
10 5F
18 60
20 61
15 62
1D 63
2B 65
7A E0
07 E1
35 E2
87 E3
7C E4
06 E5
33 E6
C6 E7
# Sample input for tab.c.
#
# This file is a part of the miguel project.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2016 Ineiev <ineiev@gnu.org>, super V 93
