; miguel hardware design: schematics PS output script
;
; miguel is free design; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program. If not, see <http://www.gnu.org/licenses/>.
;
; Copyright (C) 2009, 2016 Ineiev <ineiev@gnu.org>, super V 93
(image-color "enabled")
(gschem-use-rc-values)
;actually the filename is specified on the gschem command line
(gschem-postscript "dummyfile")
(gschem-exit)
