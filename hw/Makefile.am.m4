divert(-1)
# miguel hardware design: Makefile.am.m4
# to initially make Makefile.am, run m4 -I . Makefile.am.m4 > Makefile.am
#
# Copyright (C) 2008, 2009, 2016 Ineiev<ineiev@gnu.org>, super V 93
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
include(`m4/makefile.m4')
EXTRA_DIST=Makefile.am.m4 m4/makefile.m4 gafrc \
sym/2n7002.sym sym/avrprog.sym sym/bat54sw.sym \
sym/lmv331.sym sym/ncp583-xv.sym sym/pin.sym \
sym/pld46.sym sym/pls1.sym sym/zq.sym sym/74HC154.sym \
sym/bas16vv.sym sym/capacitor-4.sym sym/ncp583-sq.sym \
sym/nmos.sym sym/pld30.sym sym/pls15.sym sym/pmos.sym \
sym/ATmega48.sym sym/bat54s.sym sym/cap.sym sym/ncp583.sym \
sym/npn-2.sym sym/pld32.sym sym/pls16.sym sym/res.sym \
packages/0402 packages/avrprog packages/PIN packages/PLD46 \
packages/PLS16 packages/SOT23 packages/SOT563 packages/TQFP32 \
packages/0603 packages/cap5 packages/PLD30 packages/PLS1 \
packages/SC-82AB packages/SOT23_5 packages/SOT666 \
packages/0805 packages/crystal packages/PLD32 packages/PLS15 \
packages/SO24 packages/SOT323 packages/tanD

ST_BOARD(miguel)
$(srcdir)/Makefile.am: $(srcdir)/Makefile.am.m4 $(srcdir)/m4/makefile.m4
	m4 -I $(srcdir) $(srcdir)/Makefile.am.m4 > $(srcdir)/Makefile.am
schematicsdir=$(pkgdatadir)/schematics
schematics_DATA=
ST_SCHEMATIC(miguel)
