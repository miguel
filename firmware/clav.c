/* Keyboard controller: firmware for AVR.

Copyright (C) 2017 Ineiev <ineiev@gnu.org>, super V 93

This file is a part of miguel the keyboard controller.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.*/

/* Fuse bits for ATmega48..ATmega328: ext and high as default, low 0xEF.  */

#include <config.h>

#if MAX_IDLE_RATE < 0
#define RESPECT_IDLE_RATE 0
#define max_idle_rate 0
#else
#define RESPECT_IDLE_RATE 1
#define max_idle_rate MAX_IDLE_RATE
#endif

/* ADC units per battery volt.  */
#define V_BAT_SCALE (255 / 6.6)
/* Histeresis value for battery limit.  */
#define V_BAT_HIST (0.1 * V_BAT_SCALE)
#define ADC_START ((1 << ADEN) | (1 << ADSC) \
                     | (1 << ADPS2) | (1 << ADPS1)) /* 1 / 64 */

#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <string.h>

#include <avr/pgmspace.h>
#include "usbdrv.h"

enum probe_constants
{
  PROBE_SIZE_LOG = 5,
  PROBE_SIZE = 1 << PROBE_SIZE_LOG,
  PROBE_MASK = PROBE_SIZE - 1,
  PROBE_LED = 1 << 6,
  PROBE_E1 = 1 << 4,
  PROBE_E2 = 1 << 5,
  FIRST_MODIFIER = 0xE0,
  PROBE_PERIOD = 300, /* 200 us */
  LED_NUM = 4,
  EXTRA_LED = 1 << (LED_NUM - 1)
};

#include "keytab.h"

enum local_constants
{
  DIV = 192,
  PWM_RATIO = 68,
  STARTUP_SAMPLE = 60000,
  STARTUP_LEN_THR = 20,
  DATA_BUF_SIZE_LOG = 7,
  DATA_BUF_SIZE = 1 << DATA_BUF_SIZE_LOG,
  DATA_BUF_MASK = DATA_BUF_SIZE - 1,
  RNG_SAMPLE_PERIOD = 15, /* 10 us. */
  RANDOM_WIDTH = 8 /* 5 would be sufficient for scanning,
                      but adaptive_proportion test uses 8 bits.  */
};
static unsigned char startup_test_passed;
static unsigned char startup_len;
static unsigned char startup_x;
static unsigned startup_i;

static void
reset_startup (unsigned char bit)
{
  startup_x = bit;
  startup_i = startup_len = 0;
}
static void
pwm_on (void)
{
  TCCR1B = (1 << WGM13) | (1 << CS10);
  reset_startup (0);
}
static void
pwm_off (void)
{
  TCCR1B = (1 << WGM13);
}
static void
set_pwm_ratio (unsigned x)
{
  OCR1BH = ((DIV - x) >> 8) & 0xFF;
  OCR1BL = (DIV - x) & 0xFF;
  OCR1AH = (x >> 8) & 0xFF;
  OCR1AL = x & 0xFF;
}

static void
setup_pwm (void)
{
  DDRB = 6;
  ICR1H = (DIV >> 8) & 0xFF;
  ICR1L = DIV & 0xFF;
  set_pwm_ratio (PWM_RATIO);

  TCCR1A = (1 << COM1A1) | (1 << COM1B1) | (1 << COM1B0);
  pwm_on ();
}
static void
set_period (unsigned char x)
{
  OCR2A = x;
}
static unsigned char
get_period (void)
{
  return OCR2A;
}
static unsigned long ticks;
static void
inc_ticks (void)
{
  ticks += get_period ();
}
static void
setup_counter (void)
{
  /* Counter2 defines the time when counter0 measurements are to be read.  */
  /* The maximum period is 171 us (set_period (60) means 40 us);
     measured mean time between 1-to-0 transitions is 1.75 us(prototype),
     0.69us (first board).  */
  set_period (RNG_SAMPLE_PERIOD);
  TCCR2A = (1 << WGM20);
  TCCR2B = (1 << CS21) | (1 << WGM22); /* clk / 8; Top is OCR2A */

  TCCR0B = (1 << CS02) | (1 << CS01); /* Count falling edges.  */
}
static void
counter_off (void)
{
  TCCR2A = TCCR2B = 0;
  TCCR0B = 0;
}

PROGMEM const unsigned char light_addr[LED_NUM] =
{
  27 - PROBE_E1 + PROBE_E2,/* NUM */
  23 - PROBE_E1 + PROBE_E2,/* CAPS */
  31 - PROBE_E1 + PROBE_E2,/* SCROLL */
  19 - PROBE_E1 + PROBE_E2 /* BAT */
};
static unsigned char failure = 1;
static unsigned char extra_led_value;
static unsigned char lights;
static unsigned char probe_seq[PROBE_SIZE], probes[PROBE_SIZE];
static unsigned char next_probe_seq[PROBE_SIZE], next_seq_ready;
static unsigned char
get_raws (void)
{
  unsigned char x, b = PINB;

  x = PIND & 0xE3; 
  x |= (b & 1) << 4;
  x |= (b & 0x10) >> 1;
  x |= (b & 0x20) >> 3;
  return x;
}
static unsigned char
seq_to_index (unsigned char n)
{
  unsigned char r = n & 0xF;

  if (n & PROBE_E2)
    r += PROBE_E1;
  return r;
}

static unsigned char report_buf[8], report_ready = REPORT_COUNT;
static void
compile_report (void)
{
  unsigned char i, j, k, k1, x, modifiers = 0, ps;
  unsigned char next_report[sizeof (report_buf)];

  memset (next_report, 0, sizeof (report_buf));
  j = 2;
  for (i = 0; j < sizeof (report_buf) && i < sizeof (probes); i++)
    {
       ps = seq_to_index (probe_seq[i]);
       for (k = 0, k1 = 1; k < 8 && j < sizeof(report_buf); k++, k1 <<= 1) 
         {
           unsigned char l;

           if (k1 & probes[i])
             continue;
           x = k + (ps << 3);
           if (RAW_SCANCODES)
             {
               next_report[j++] = x;
               continue;
             }
           x = scan_to_code (x);
           if (x >= FIRST_MODIFIER)
             {
               modifiers |= 1 << (x - FIRST_MODIFIER);
               continue;
             }
           /* Sort by keycode to make the order deterministic.
              The order is reversed to put error codes to the end.  */
           for (l = j; next_report[l - 1] < x && l > 2; l--)
             next_report[l] = next_report[l - 1];
           next_report[l] = x;
           j++;
         }
    }
  next_report[0] = modifiers;
  if (memcmp (report_buf, next_report, sizeof (report_buf)))
    {
      unsigned char vex = !0;

      if (ENABLE_JAMMING && next_report[4]) /* Don't pass more than 2 keys. */
        vex = 0;
      if (vex)
        {
          memmove (report_buf, next_report, sizeof (report_buf));
          report_ready = REPORT_COUNT;
        }
    }
  if (next_seq_ready)
    {
      memmove (probe_seq, next_probe_seq, PROBE_SIZE);
      next_seq_ready = 0;
    }
}
static unsigned char
assign_extra_led (void)
{
  static unsigned char dec, cnt, flip;
  unsigned char l = lights & ~EXTRA_LED;

  if (!(0x3 & dec++))
    cnt--;
  if (!cnt)
    {
      if (flip)
        cnt = 128;
      else
        cnt = extra_led_value;
      flip = !flip;
    }
  if (failure || flip)
    l |= EXTRA_LED;
  return l;
}
static unsigned char voltage_is_low;
static unsigned char adc_voltage;
static unsigned char voltage_limit;
static char
check_adc (void)
{
  unsigned char x;

  if (ADCSRA & (1 << ADSC))
    return 1;
  x = ADCH;
  ADCSRA = ADC_START;
  if (MIN_VOLTAGE >= 0)
    voltage_is_low = (x < voltage_limit);
  if (x > 174)
    {
      extra_led_value = 255;
      return 0;
    }
  if (x < 128)
    {
      extra_led_value = 71;
      return 0;
    }
  extra_led_value = (x - 128) * 4 + 71;
  return 0;
}
static void
phase_probe (void)
{
  static unsigned t;
  static unsigned char ph;
  unsigned char a, li;
  unsigned ti = ticks;

  if (ti - t < PROBE_PERIOD)
    return;

  check_adc ();

  li = assign_extra_led ();

  t = ti;

  probes[ph] = get_raws ();
  if (probe_seq[ph] & PROBE_LED)
    probes[ph] = ~0;
  ph++;
  if (ph >= sizeof (probe_seq))
    {
      ph = 0;
      compile_report ();
    }
  a = probe_seq[ph];
  if (a & PROBE_LED)
    {
      unsigned char pc = PORTC, b = a - PROBE_LED;

      if (b < LED_NUM)
        {
          memcpy_P (&a, light_addr + b, 1);
          if ((1 << b) & li)
            a = pc; /* Don't change PORTC to let the LED glow.  */
        }
      else
        a = pc; /* The signal is not used; don't change PORTC.  */
    }
  PORTC = a;
}
static void
init_probe_seq (void)
{
  unsigned char i, j, la[LED_NUM], v;
#if UNUSED_COLUMNS && !RAW_SCANCODES
  unsigned char u[UNUSED_COLUMNS];

  memcpy_P (u, unused_columns, sizeof (u));
#endif
  memcpy_P (la, light_addr, sizeof (la));

  for (i = 0; i < 16; i++)
    probe_seq[i] = i + PROBE_E1;
  for (; i < 32; i++)
    probe_seq[i] = i - PROBE_E1 + PROBE_E2;
  for (i = 0; i < 32; i++)
    {
      v = 0;
      for (j = 0; j < sizeof (la); j++)
        if (la[j] == probe_seq[i])
          {
            probe_seq[i] = j + PROBE_LED;
            v = !0;
            break;
          }
      if (v)
        continue;
#if UNUSED_COLUMNS && !RAW_SCANCODES
      for (j = 0; j < sizeof (u); j++)
        if (u[j] == probe_seq[i])
          {
            probe_seq[i] = LED_NUM + PROBE_LED;
            break;
          }
#endif
    }
  memmove (next_probe_seq, probe_seq, PROBE_SIZE);
}
static void
setup_probe (void)
{
  init_probe_seq ();
}
static void
startup_health_test (unsigned char bit)
{
  if (startup_i >= STARTUP_SAMPLE)
    {
      startup_test_passed = startup_len < STARTUP_LEN_THR;
      if (startup_test_passed)
        failure = 0;
      reset_startup (bit);
      return;
    }
  startup_i++;
  if (bit != startup_x)
    {
      startup_len = 0;
      startup_x = bit;
      return;
    }
  if (++startup_len > STARTUP_LEN_THR)
    reset_startup (bit);
}
static void
setup_adc (void)
{
  voltage_limit = (unsigned char)(MIN_VOLTAGE * V_BAT_SCALE + .5);
  ADMUX = (1 << REFS0) | (1 << ADLAR) | 6; /* AVcc as reference, Left-adjusted, ADC6.  */
  ADCSRA = ADC_START;
}
static void
adc_off (void)
{
  ADCSRA = 0;
}
static unsigned char data_buf[DATA_BUF_SIZE], head, tail;
static unsigned char to_transmit;
/* The next two function implement sufficient continuous checks
   per NIST SP800-90b draft.  */
/* NIST SP800-90b draft, 4.4.1.  */
static unsigned char
repetition_count (unsigned char bit)
{
  static unsigned char prev, cnt;

  if (prev == bit)
    {
      cnt++;
      return cnt >= 41; /* Cutoff value for 1 bit per sample.  */
    }
  cnt = 0;
  prev = bit;
  return 0;
}
/* NIST SP800-90b draft, 4.4.2.
   Only correct when RANDOM_WIDTH == 8 && DATA_BUF_SIZE == 128.  */
static unsigned char
adaptive_proportion (unsigned char h0, unsigned char h)
{
  static unsigned ap_bits;
  unsigned char j, k, d, d0, res = 0;

  if (!head && !ap_bits)
    {
      /* Initialize ap_bits when the buffer is full for the first time.  */
      for (j = 0; j < DATA_BUF_SIZE; j++)
        for (k = 0, d = data_buf[j]; k < RANDOM_WIDTH; k++, d >>= 1)
          ap_bits += d & 1;
      return 0;
    }
  if (!ap_bits) /* if !ap_bits, we have either total failure or no enough data.  */
    return 0;
  /* ap_bits has been initialized; run the test.  */
  d0 = data_buf[head];
  d = data_buf[h0];
  for (k = 0; k < RANDOM_WIDTH; k++, d >>= 1, d0 >>= 1)
    {
      ap_bits += (d & 1) - (d0 & 1);
      /* 624 is for 1 bit of assessed entropy per sample; strictly
         speaking, we should only check either ap_bits >= 624
         or ap_bits <= 1024 - 624 depending on the starting bit.  */
      if (ap_bits >= 624 || ap_bits <= 1024 - 624)
        res = 1;
    }
  return res;
}
/* This procedure can't produce the strictly uniform distribution
   (the probability of every permutation is an integer number of
    (1/n)^n, where n is the size of sequence, which doesn't divide (1/n!)),
   but it may suffice.  */
static void
shuffle_next_seq (unsigned char x)
{
  static unsigned char seq_no, y;

  if (next_seq_ready)
    return;

  x &= PROBE_MASK;
  y = next_probe_seq[seq_no];
  next_probe_seq[seq_no] = next_probe_seq[x];
  next_probe_seq[x] = y;
  seq_no++;
  if (seq_no < sizeof (next_probe_seq))
    return;
  seq_no = 0;
  next_seq_ready = !0;
}
/* Setting idle_rate to 0 (or generally large values) may provide
   the attacker some information based on keypress timings;
   --disable-idle-rate will switch it off and save about 200 bytes in flash.  */
static unsigned char idle_rate = 500 / 4;
static unsigned long idle_period;
static unsigned char protocol;
static void
reset_idle_period (void)
{
  if (!RESPECT_IDLE_RATE)
    return;
  idle_period = idle_rate * 6000ul; /* Depends on T2 prescaler setting.  */
  idle_period += ticks;
}
static void
count_idle_period (void)
{
  if (!RESPECT_IDLE_RATE)
    return;
  if (report_ready || !idle_rate)
    return;
  if (idle_period - ticks < 1ul<<31)
    return;
  report_ready = REPORT_COUNT;
}
static void
signal_failure (void)
{
  startup_test_passed = 0;
  failure = 1;
  reset_startup (0);
}
static void
run_ap (unsigned char x)
{
  unsigned char h0 = head;

  data_buf[head++] = x;
  head &= DATA_BUF_MASK;
  if (adaptive_proportion (h0, head))
    signal_failure ();
}
static void
payload_loop (void)
{
  static unsigned char i, x, next, cnt0;
  unsigned char cnt, cnt1;

  wdt_reset ();
  if (!(TIFR2 & (1 << TOV2)))
    return;

  TIFR2 = 1 << TOV2;
  inc_ticks ();
  cnt = TCNT0;

  phase_probe ();
  count_idle_period ();
  if (cnt0 == cnt)
    /* No transitions since last check, consider total failure per AIS 31.  */
    signal_failure ();
  cnt1 = cnt0;
  cnt0 = cnt;
  if (OUTPUT_TCNT0)
    /* Output TCNT0 value instead of real bits -- use for evaluations.  */
    x = cnt - cnt1;
  else
    {
      x <<= 1;
      next ^= cnt & 1;
      x |= next;

      if (!startup_test_passed)
        startup_health_test (next);
      if (repetition_count (next))
        signal_failure ();

      if (++i < RANDOM_WIDTH)
        return;
    }
  i = 0;
  shuffle_next_seq (x);
  run_ap (x);
  x = 0;
}

PROGMEM const char usbHidReportDescriptor[USB_CFG_HID_REPORT_DESCRIPTOR_LENGTH] =
{
  0x05, 0x01, /* USAGE_PAGE (Generic Desktop) */
  0x09, 0x06, /* USAGE (Keyboard) */
  0xA1, 0x01, /* COLLECTION (Application) */
#if 0
  /* With our test utility, it doesn't matter if this section is defined.  */
  0x15, 0x00, /*   LOGICAL_MINIMUM (0) */
  0x26, 0xFF, 0x00, /*   LOGICAL_MAXIMUM (255) */
  0x75, 0x08, /*   REPORT_SIZE (8) */
  0x95, 0x80, /*   REPORT_COUNT (128) */
  0x09, 0x00, /*   USAGE (Undefined) */
  0xb2, 0x02, 0x01, /* FEATURE (Data,Var,Abs,Buf) Data to transmit */
#endif
  0x05, 0x07, /*   USAGE_PAGE (Key Codes) */
  0x19, 0xE0, /*   USAGE_MINIMUM (224) */
  0x29, 0xE7, /*   USAGE_MAXIMUM (231) */
  0x15, 0x00, /*   LOGICAL_MINIMUM (0) */
  0x25, 0x01, /*   LOGICAL_MAXIMUM (1) */
  0x75, 0x01, /*   REPORT_SIZE (1) */
  0x95, 0x08, /*   REPORT_COUNT (8) */
  0x81, 0x02, /*   INPUT (Data,Var,Abs) Modifier byte */
  0x95, 0x01, /*   REPORT_COUNT (1) */
  0x75, 0x08, /*   REPORT_SIZE (8) */
  0x81, 0x01, /*   INPUT (Constant) Reserved byte: expected in the boot mode */
  0x95, 0x05, /*   REPORT_COUNT (5) */
  0x75, 0x01, /*   REPORT_SIZE (1) */
  0x05, 0x08, /*   USAGE_PAGE (LEDs) */
  0x19, 0x01, /*   USAGE_MINIMUM (1) */
  0x29, 0x05, /*   USAGE_MAXIMUM (5) */
  0x91, 0x02, /*   OUTPUT (Data,Var,Abs) LED report */
  0x95, 0x01, /*   REPORT_COUNT (1) */
  0x75, 0x03, /*   REPORT_SIZE (3) */
  0x91, 0x01, /*   OUTPUT (Constant) LED report padding */
  0x95, 0x06, /*   REPORT_COUNT (6) */
  0x75, 0x08, /*   REPORT_SIZE (8) */
  0x15, 0x00, /*   LOGICAL_MINIMUM (0) */
  0x25, 0x65, /*   LOGICAL_MAXIMUM (101) */
  0x05, 0x07, /*   USAGE_PAGE (Key Codes) */
  0x19, 0x0,  /*   USAGE_MINIMUM (0) */
  0x29, 0x65, /*   USAGE_MAXIMUM (101) */
  0x81, 0x00, /*   INPUT (Data,array) Key arrays (6 bytes) */
  0xC0,       /* END COLLECTION */
};

#if RNG_OUTPUT
uchar
usbFunctionRead (uchar *data, uchar len)
{
  unsigned char l, l1;

  if (len > to_transmit)
    len = to_transmit;
  l = len;
  if (tail + l > DATA_BUF_SIZE)
    {
      l1 = DATA_BUF_SIZE - tail;
      memmove (data, data_buf + tail, l1);
      data += l1;
      l = l - l1;
      tail = 0;
    }
  memmove (data, data_buf + tail, l);
  tail += l;
  tail &= DATA_BUF_MASK;
  to_transmit -= len;
  return len;
}
#endif
uchar
usbFunctionWrite (uchar *data, uchar len)
{
  if (len == 1)
    lights = data[0];
  else if (HAVE_NOISE_CONTROL)
    {
      set_period (data[0]);
      if (data[1])
        pwm_on ();
      else
        pwm_off ();
    }
  return len;
}

enum USB_HID_constants
{
  USBRQ_GET_INPUT = 1,
  USBRQ_GET_FEATURE = 3
};
usbMsgLen_t
usbFunctionSetup (uchar data[8])
{
  usbRequest_t *rq = (void *)data;

  if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS)
    {
      if(rq->bRequest == USBRQ_HID_GET_REPORT)
        {
          if (RNG_OUTPUT && rq->wValue.bytes[1] == USBRQ_GET_FEATURE)
            {
              to_transmit = (head - tail) & DATA_BUF_MASK;
              if (!to_transmit)
                return 0;
              return USB_NO_MSG;
            }
          if ((!RNG_OUTPUT) || rq->wValue.bytes[1] == USBRQ_GET_INPUT)
            {
              if (!report_ready)
                return 0;
              usbMsgPtr = report_buf;
              if (RESPECT_IDLE_RATE)
                {
                  report_ready--;
                  reset_idle_period ();
                }
              return sizeof(report_buf);
            }
        }
      else if(rq->bRequest == USBRQ_HID_SET_REPORT)
        return USB_NO_MSG;
      else if(rq->bRequest == USBRQ_HID_GET_IDLE)
        {
          usbMsgPtr = &idle_rate;
          return 1;
        }
      else if(rq->bRequest == USBRQ_HID_SET_IDLE)
        {
          idle_rate = rq->wValue.bytes[1];
          if (RESPECT_IDLE_RATE && max_idle_rate
              && (!idle_rate || max_idle_rate < idle_rate))
            idle_rate = max_idle_rate;
          reset_idle_period ();
        }
      else if(rq->bRequest == USBRQ_HID_GET_PROTOCOL)
        {
          usbMsgPtr = &protocol;
          return 1;
        }
      else if(rq->bRequest == USBRQ_HID_SET_PROTOCOL)
        protocol = rq->wValue.bytes[1];
    }
  return 0;
}
static void
macro_delay (void)
{
  unsigned char t = (ticks >> 16), t1;

  /* 262..306 ms */
  do
    { 
      t1 = ticks >> 16;
      payload_loop ();
    }
  while (t1 - t < 7);
}
static void
vdd_off (void)
{
  /* No need to actually pull down the signals;
     high-impedance state will do.  */
  DDRC = 0;
}
static void
vdd_on (void)
{
  DDRC = 0x3F;
}
static void
power_off (void)
{
  pwm_off ();
  DDRB = 0;
  counter_off ();
  vdd_off ();
  /* The 1.5k resistor would draw about 2 mA, so we don't disconnect
     when sleeping.  */
  usbDeviceConnect();
}
static void
power_on (void)
{
  vdd_on ();
  setup_pwm ();
  setup_counter ();
  setup_probe ();
}
static unsigned char
usb_present (void)
{
  /* High level on Vusb.  */
  return PINB & (1 << 3);
}
static void
setup_wdt (void)
{
  wdt_reset ();
  WDTCSR |= (1 << WDE) | (1 << WDCE);
/* 1s timeout.  */
  WDTCSR = (1 << WDE) | (1 << WDP2) | (1 << WDP1);
}
static unsigned char
check_voltage (void)
{
  unsigned x;

  if (MIN_VOLTAGE < 0)
    return !usb_present ();
  for (x = 0; x < 0x11; x++) /* Wait for ADC to settle.  */
    while(check_adc ());

  return voltage_is_low || !usb_present ();
}
static void
main_loop (void)
{
  cli ();
  power_off ();
  setup_wdt ();
  setup_adc ();
  if (check_voltage ())
    while (1)
      {
        adc_off ();
        /* No USB connection: sleep until watchdog resets the MCU.  */
        SMCR = (1 << SM1) | (1 << SE); /* Power-down mode, enable sleep.  */
        sei ();
        sleep_cpu ();
      }
  if (MIN_VOLTAGE >= 0)
    voltage_limit -= (unsigned char)V_BAT_HIST;
  usbDeviceDisconnect ();
  power_on ();
  macro_delay ();
  usbDeviceConnect ();
  reset_idle_period ();
  sei ();

  while (usb_present () && !voltage_is_low)
    {
      payload_loop ();
      usbPoll ();
      if (!(report_ready && usbInterruptIsReady ()))
        continue;
      usbSetInterrupt ((void *)report_buf, sizeof (report_buf));
      if (!RESPECT_IDLE_RATE)
        continue;
      if (report_ready)
        report_ready--;
      reset_idle_period ();
    }
}
int
main (void)
{
  MCUCR = 1 << PUD;
  usbInit ();
  while (1)
    main_loop ();
  return 0;
}
